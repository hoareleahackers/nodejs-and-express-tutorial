const http = require('http');

const requestHandler = (request, response) => {
    const method = request.method;
    const url = request.url;
    const headers = request.headers;
    var userAgent = headers['user-agent'];
    let name = "Frederique Lemieux";

    // const output = `Request to the server came at ${new Date()}.`;
    // response.end(output);

    if (url === '/'){
        // Do with the base url, we don't care what method is used, we'll just return some html
        response.write('<html><body>');
        response.write('<h1>Hello, World!</h1><br><h2>MainPage</h2>');
        response.end('<h3>Requested url ' + url + '</h3></body></html>');
    } else if (url === '/name') {
        // Do something else at the name path of you server
        if (method === 'GET') {
            response.end('Hey ' + name + ', welcome back.  If that isn\'t your name then, please submit a new name using POST');
        } else if (method === 'POST') {
            console.log(headers);
            let body = "";
            if (headers['content-type'] === 'application/json') {
                request.on('data', chunk => {
                    body += chunk.toString(); // convert Buffer to string
                });

                request.on('end', () => {
                    // Parse the body into JSON
                    body = JSON.parse(body);
                    name = body.firstName + " " + body.lastName;
                    response.end('Thanks, ' + name + ' the default name has now been updated.');
                });
            }
        }
    } else {
        // return an error if any other path is requested
        response.end('Something went wrong')
    }

};

const server = http.createServer(requestHandler);

console.log('listening on port 9999');

server.listen(9999);
