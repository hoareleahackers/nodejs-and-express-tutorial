const express = require('express');
const router = express.Router();

const DarkSky = require('dark-sky');
const darksky = new DarkSky('ca234379c81eb9c88c8f4f7b2da8a580'); // Your API KEY can be hardcoded, but I recommend setting it as an env variable.
//
// darksky
//     .latitude('37.8267')            // required: latitude, string || float.
//     .longitude(-122.423)            // required: longitude, string || float.
//     .time('2016-01-28')             // optional: date, string 'YYYY-MM-DD'.
//     .units('ca')                    // optional: units, string, refer to API documentation.
//     .language('en')                 // optional: language, string, refer to API documentation.
//     .exclude('minutely,daily')      // optional: exclude, string || array, refer to API documentation.
//     .extendHourly(true)             // optional: extend, boolean, refer to API documentation.
//     .get()                          // execute your get request.
//     .then(console.log)
//     .catch(console.log);             // handle your error response.

router.get('/', (req, res) => {
    console.log(req.query.latitude);
    res.send('success');
});

module.exports = router;
