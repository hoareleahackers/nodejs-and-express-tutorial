var express = require('express');
var router = express.Router();

let name = "Frederique Lemieux";

/* GET name page. */
router.get('/', function(req, res, next) {
    res.send(200,'Hey ' + name + ', welcome back.  If that isn\'t your name then, please submit a new name using POST');
});

/* POST name page. */
router.post('/', function(req, res, next) {
    name = req.body.firstName + " " + req.body.lastName;
    res.send(200,'Thanks, ' + name + ' the default name has now been updated.');
});

module.exports = router;
