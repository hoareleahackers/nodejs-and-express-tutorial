#Workshop 01
In this workshop we'll:
 - build a basic server in NodeJS.  
 
 The reason we we'll do this in NodeJS firstly using only NodeJS you can't hide behind frameworks, you need to program all the logic yourself, this will give you a much greater understanding of what is actually going on.  Second, it'll illustrate why you'd want to use a external library like express.  
 
 Then when you use express, hopefully you'll be more aware of what is actually happening under the hood.  

- We'll learn about the basics of http requests, methods and urls. 
- Then we'll show you how to do the same thing in Express and discuss the pro/cons of using a library on to of NodeJS.  
- We'll then import an npm package to show you how that would work, and get our first API to do something a little more interesting.

## Basic http server in NodeJS
```javascript
var http = require('http');

var server = http.createServer(function(request, response) {

  // process request
  // send back the response
  
});

server.listen(9999);
```
or maybe this is more readable: 
```javascript
const http = require('http');

const requestHandler = (request, response) => {

    const output = `Request to the server came at ${new Date()}.`; 
    response.end(output);
    
};

const server = http.createServer(requestHandler);

server.listen(9999);
```

It also makes your code easier to test, but that is for another session:

Now if you run your server using node \<javascript file name>, then go to you browser and navigate to [http://localhost:9999](http://localhost:9999), you should see the response in your broswer.

## Call-whats? 

In Javascript you can do something like this, which is called a [callback](https://en.wikipedia.org/wiki/Callback_(computer_programming)):
```javascript
function hello(name) {

  console.log("Hello ", name);
  
}

function greet(name, callback) {

  callback(name);
  
}

greet("Michael", hello);
```

You can also chain createServer and listen methods:

```javascript
var http = require('http');

http.createServer(handler).listen(9999);

function handler(request, response) {
  console.log("Hello again!");
}
```

## Using Method, URL and Headers
- **Methods:** Http requests have [different methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods) (GET, POST, etc.) and it is important to know which is being used so that your server can act accordingly.
- **URLs:** Http request can also access different [URLs](https://en.wikipedia.org/wiki/URL) which point to different parts of you server/API, so we need to figure out where the request has been made to.  We can use the url property of the request for this.
- **Headers:** [HTTP headers](https://code.tutsplus.com/tutorials/http-headers-for-dummies--net-8039) allow the client and the server to pass additional information with the request or the response. This can often contain the method, [http status codes](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes), [content type](https://stackoverflow.com/questions/23714383/what-are-all-the-possible-values-for-http-content-type-header) and much more.

You can access these like this:
```javascript
var method = request.method;
var url = request.url;
var headers = request.headers;
var userAgent = headers['user-agent'];
```

Perhaps you'd like to perform different operations if the request is a get or post request:
```javascript
if (method === 'GET') {
    // Respond to get request
    
} else if (method === 'POST') {
    // Respond to post request
    
}
```

Perhaps you want to check which url was called:
```javascript
if (url === '/'){ 
    // Do with the base url
    
} else if (url === '/name') {
    // Do something else at the name path of you server
    
} else {
    // return an error if any other path is requested
    
}
```

To check what kind of data is being transferred do something like this:
```javascript
if (headers['content-type'] === 'text/html') {
    // Do something
}
```

## Response
To respond to requests, it is easy enough, you can construct your response with various methods such as *.writeHead* (for headers) and *.write* for writing lines into the response.  Then when you are happy with the response, you use *.end* to post your data back.

```javascript
response.writeHead(200, {'content-type': 'text/html'}); 

	if (url === '/') {
	    
		response.write('<html><body>');
		response.write('<h1>Hello, World!</h1><br><h2>MainPage</h2>');
		response.end('<h3>Requested url ' + url + '</h3></body></html>');
    
	} else if (url === '/me') {
  
		response.write('<html><body><h1>Hello, World!</h1><br><h2>About me</h2>');
		response.end('<h3>Requested url ' + url + '</h3></body></html>');
    
	} else {
  
		response.write('<html><body><h1>Error</h1><br>');
		response.end('<h3>No such a page ' + url + '</h3></body></html>');
    
	}
```
## Testing different request methods
So if you have been following this guide and trying out the steps, you'll notice that whenever you use your browser request a URL, your browser only does GET requests.  
This is normal because that is what a browser does, it GETs web pages for you.  So how do you test other methods.  Well, you could go and program your get request in your terminal window, or using Python, NodeJS or any other capable programming language.  But the easiest way is to use something like [Postman](https://www.getpostman.com).  Postman is a superb piece of software, and well worth learning how to use it.  I'll leave it up to you to watch some tutorials.  In this example you'll need to set up a POST request, which will post first and last name as a JSON string.  

## Request Body
Things have been easy enough until now.  Things get a little harder when you need to read data.  Because data doesn't arrive all at once, but rather in little chunks you need to take these chunks of data and reconstruct the original data.  This is where something like ExpressJS really shines
```javascript
if (req.method === 'POST') {
    let body = '';
    req.on('data', chunk => {
        body += chunk.toString(); // convert Buffer to string
    });
    req.on('end', () => {
        console.log(body);
        res.end('ok');
    });
}
```

## Introducing ExpressJS
Well, hopefully by now you'll have a basic understanding of how to create a server in NodeJS, and understand the basics of server requests like methods and headers.  Now lets do the same but using ExpressJS.  

There is a bit more involved in creating the basic code to set up your express server, but luckily you don't have do this, we'll be using [express generator](https://expressjs.com/en/starter/generator.html) to quickly create an application skeleton.  There will be parts of the generated code which we will note cover in the workshop, I think it is best to keep things as simple as possible when starting.

First let's install express-generator globally using the following command in you terminal window or command prompt:
```
npm install express-generator -g
```

Now let's create our first express skeleton app using the command:
```express --no-view myFirstExpressApp```

cd into the created directory and install the dependencies:

```cd myFirstExpressApp``` and then ```npm i```

Now run the server using ```npm start```

Go to [http://localhost:3000](http://localhost:3000), boom! there you go, four commands and you have a working server.

##Express Router
So what is happening when you run your server and navigate to the route path of the server? 

Go into your bin/www file, and try to read through what is happening.  You should recognise the *.createServer* and *.listen* methods, but, there is obviously a lot more going on. Most of it you can ignore, but you'll see that it is passing your app module as an argument for the *.createServer* method. 

So let's navigate to the **app.js** file and see what is happening there.  Again there is a lot you can ignore for now, but the two lines you are interested in are:
```javascript
app.use('/', indexRouter);
app.use('/users', usersRouter);
```

These are routing your requests depending on whether you are requesting the root or /users path.

You can see that this express app has split the routes (or paths) of the application into logical bite sized javascript files,  rather than having lots of if then style logic like we had in our NodeJS app.

If you look whether the *indexRouter* and *usersRouter* methods are coming from, via the require function at the top of the file, you can investigate this further, by looking in the */routes* folder of the project.

Look in the index.js route and the code will look something like this:
```javascript
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
```

Express is nice in that you simply need to use the *.get* method to do something with a GET request.  In this case it is returning the index page which is located in the */public* folder of the express app. So let's build or own names route, in the **app.js** file, add these two lines:

```javascript
var nameRouter = require('./routes/name');

app.use('/name', nameRouter);
```

Create a file called **name.js** in the */routes* folder.  Copy the code from index.js and then in that file you can paste the following code:
```javascript
let name = "Frederique Lemieux"
/* GET name page. */
router.get('/', function(req, res, next) {
    res.send(200,'Hey ' + name + ', welcome back.  If that isn\'t your name then, please submit a new name using POST');
});

/* POST name page. */
router.post('/', function(req, res, next) {
    name = req.body.firstName + " " + req.body.lastName;
    res.send(200,'Thanks, ' + name + ' the default name has now been updated.');
});
```

Hopefully you can see that what took approx 20 lines of code using NodeJS, took 6 lines using Express.  Pretty cool, eh? 

## Different ways of sending data to the server
There are many ways of sending data to a server, but three are generally the most popular.  
- **Posting JSON string:** we have already used this content-type (application/json) above
- **Placing data in the URL paths:** This is where you have data directly in the URL string.  You should be careful not to do this with sensitive information (username and passwords, financial information etc.) as this will be saved in your browser's history. This could be something like:
*http://localhost:3000/weather/edinburgh* where you want to get the city name, in this case *Edinburgh* out of the URL path.  This is easy, simply use the semi colon and a name in the router path, and then use that variable using ```request.params.<variable name>```.  This peels the parameters out of the request.
```javascript
router.get('/weather/:city', function(req, res) {
    let city = req.params.city
    console.log(city);  // For the example above this will log 'edinburgh'
    });
```
- **URL Query Strings:** Have you ever noticed when you are browsing the internet, in the address bar a website url has something like this at the end of it *websitename.com?firstName=Fred&lastName=Lemieux&age=34* Well, that's a query string.  It is just a bunch of name/value pairs, i.e. in the previous example the property age, has the value 34.
So how do we extract this data from the query string?  Simple, in Express you can just use the request.query parameter 
```javascript
router.get('/', (req, res) => {
    console.log(req.query.name);  //This will return Fred
});
```



## Integrating Other NPM modules and APIs
We are now going to do something a fun.  We are going to use a postcode npm package, to allow us take a given post code and return an address.  To do this we are going to use this [postcode npm package](https://darksky.net/).

## What's next
What API would you like to build, let's get some ideas, and perhaps through these tutorials we can begin to build something actually useful.  

I'd like so also know what you'd like to learn going forward, here are some ideas:

- Unit and Integration Testing in NodeJS
- Using GIT for version control and collaborative programming
- Calling other APIs and using the data
- Testing APIs, and creating API docs using Postman
- Creating ReactJS web pages
- Storing data in a NoSQL database
- How to use other people's APIs  

# Node.js v8 issue with http

https://github.com/nodejs/node/issues/13461

### Resources
[Anatomy of an HTTP Transaction](https://nodejs.org/en/docs/guides/anatomy-of-an-http-transaction)

[Node Girls Express Workshop](https://github.com/node-girls/express-workshop/blob/master/step01.md)

[Learn You Node](https://www.github.com/workshopper/learnyounode)

### Thanks
Very special thanks to Michael Antczac for his NodeJS meetups which has laid the foundation for these workshops. 
